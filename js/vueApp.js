var app = new Vue({
	el: '#app',
	data:{
		Color:['#F4E8C1','#DDCECD','#A9D0F5','#D4DCCD','#DECDF5','#D8D8D8','#B8D0EB','#F6E4F6','#DFD6A7','#BCA9F5'],
		Matriz: 'FODA',
		Foda:{
			DatoFoda: '',
			TipoDato: 'Fortaleza',
			Fortalezas: [],
			Oportunidades: [],
			Debilidades: [],
			Amenazas: []
		},
		Mpc:{
			Empresas:[],
			Ponderaciones:[
				{"valor": 0.11},
				{"valor": 0.11},
				{"valor": 0.11},
				{"valor": 0.11},
				{"valor": 0.11},
				{"valor": 0.11},
				{"valor": 0.11},
				{"valor": 0.11},
				{"valor": 0.12}
			],
			NumEmp: 2,
			Merito:[],
			Mensajito: '',
			ColorValido:'#0008ff'
		},
		Amofhit:{},
		Peyea:{
			Pei:{
				FuFi:[
					{"nombre": 'Rendimiento sobre la inversión',"valor": parseInt(1)},
					{"nombre": 'Apalancamiento',"valor": parseInt(1)},
					{"nombre": 'Liquidez',"valor": parseInt(1)},
					{"nombre": 'Capital de trabajo',"valor": parseInt(1)},
					{"nombre": 'Flujos de efectivo',"valor": parseInt(1)},
					{"nombre": 'Facilidad para salir del mercado',"valor": parseInt(1)}
				],
				VeCo:[
					{"nombre": 'Participación en el mercado',"valor": parseInt(-6)},
					{"nombre": 'Calidad del producto',"valor": parseInt(-6)},
					{"nombre": 'Ciclo de vida del producto',"valor": parseInt(-6)},
					{"nombre": 'Lealtad de los clientes',"valor": parseInt(-6)},
					{"nombre": 'Conocimientos tecnológicos',"valor": parseInt(-6)},
					{"nombre": 'Control sobre los proveedores y distribuidores',"valor": parseInt(-6)}
				]
			},
			Pee:{
				EsAm:[
					{"nombre": 'Cambios tecnológicos',"valor": parseInt(-6)},
					{"nombre": 'Tasa de inflación',"valor": parseInt(-6)},
					{"nombre": 'Variabilidad de la demanda',"valor": parseInt(-6)},
					{"nombre": 'Barreras para entrar en el mercado',"valor": parseInt(-6)},
					{"nombre": 'Presión competitiva',"valor": parseInt(-6)},
					{"nombre": 'Elasticidad de la demanda',"valor": parseInt(-6)}
				],
				FuIn:[
					{"nombre": 'Potencial de crecimiento',"valor": parseInt(1)},
					{"nombre": 'Potencial de utilidades',"valor": parseInt(1)},
					{"nombre": 'Aprovechamiento de recursos',"valor": parseInt(1)},
					{"nombre": 'Acceso a nuevas tecnologias',"valor": parseInt(1)},
					{"nombre": 'Facilidad para entrar en el mercado',"valor": parseInt(1)},
					{"nombre": 'Productividad, aprovechamiento de la capacidad',"valor": parseInt(1)}
				]
			},
			Promedio:{
				FuFi: 0,
				VeCo: 0,
				EsAm: 0,
				FuIn: 0
			},
			Datos:{
				x: 0,
				y: 0
			}
		}
	},
	methods:{
		// Metodos Generales
		FormatearNumero: function(x, posiciones = 0){
			var s = x.toString();
			var l = s.length;
			var decimalLength = s.indexOf('.') + 1;
			var numStr = s.substr(0, decimalLength + posiciones);
			return Number(numStr);
		},
		// Metodos FODA
		Agregar: function(){
			if((this.Foda.DatoFoda).trim() != '' && this.Foda.TipoDato != ''){
				switch(this.Foda.TipoDato){
					case 'Fortaleza':
						this.Foda.Fortalezas.push(this.Foda.DatoFoda);
						break;
					case 'Oportunidad':
						this.Foda.Oportunidades.push(this.Foda.DatoFoda);
						break;
					case 'Debilidad':
						this.Foda.Debilidades.push(this.Foda.DatoFoda);
						break;
					case 'Amenaza':
						this.Foda.Amenazas.push(this.Foda.DatoFoda);
						break;
				}
				this.Foda.DatoFoda = '';
				document.getElementById('DatoFoda').focus();
			}else{
				document.getElementById('DatoFoda').focus();
			}
		},
		CambiarMatriz: function(dato){
			this.Matriz = dato;
		},
		Generar: function(){
			const { jsPDF } = window.jspdf;
			const { html2canvas } = window.html2canvas;
			var ancho = document.getElementById("Foda").offsetWidth;
			var alto = document.getElementById("Foda").offsetHeight + 100;
			const pdf = new jsPDF({
				orientation: "l",
				unit: "mm",
				format: [ancho, alto]
			});

			pdf.html(document.getElementById('Foda'),{
				callback: function (doc){
					doc.save()
				},
			});
		},
		Captura: function(){
			html2canvas(document.querySelector("#Foda"),{scale:3}).then(canvas => {
				var a = document.createElement('a');
				a.href = canvas.toDataURL("image/jpg").replace("image/jpg", "image/octet-stream");
				a.download = 'Matriz.jpg';
				a.click();

				// texto1.style.transform = 'rotate(180deg)';
				// texto2.style.transform = 'rotate(180deg)';
			});
		},
		// Metodos MPC
		CrearEmpresas: function(){
			this.Mpc.Empresas = [];
			for (var i = 1; i <= this.Mpc.NumEmp; i++){
				var empresa = {
					"Nombre": 'Empresa ' + i,
					"c01": 0,
					"c02": 0,
					"c03": 0,
					"c04": 0,
					"c05": 0,
					"c06": 0,
					"c07": 0,
					"c08": 0,
					"c09": 0,
					"p01": 0,
					"p02": 0,
					"p03": 0,
					"p04": 0,
					"p05": 0,
					"p06": 0,
					"p07": 0,
					"p08": 0,
					"p09": 0,
					"SumaP": 0
				}
				this.Mpc.Empresas.push(empresa);
			}
			this.Mpc.Merito = [];
			this.Mpc.Mensajito = '';
		},
		PuntuarEmpresa: function(){
			for (var dato = 0; dato < this.Mpc.Empresas.length; dato++) {
				this.Mpc.Empresas[dato].p01 = parseFloat(this.Mpc.Empresas[dato].c01) * this.Mpc.Ponderaciones[0].valor;
				this.Mpc.Empresas[dato].p02 = parseFloat(this.Mpc.Empresas[dato].c02) * this.Mpc.Ponderaciones[1].valor;
				this.Mpc.Empresas[dato].p03 = parseFloat(this.Mpc.Empresas[dato].c03) * this.Mpc.Ponderaciones[2].valor;
				this.Mpc.Empresas[dato].p04 = parseFloat(this.Mpc.Empresas[dato].c04) * this.Mpc.Ponderaciones[3].valor;
				this.Mpc.Empresas[dato].p05 = parseFloat(this.Mpc.Empresas[dato].c05) * this.Mpc.Ponderaciones[4].valor;
				this.Mpc.Empresas[dato].p06 = parseFloat(this.Mpc.Empresas[dato].c06) * this.Mpc.Ponderaciones[5].valor;
				this.Mpc.Empresas[dato].p07 = parseFloat(this.Mpc.Empresas[dato].c07) * this.Mpc.Ponderaciones[6].valor;
				this.Mpc.Empresas[dato].p08 = parseFloat(this.Mpc.Empresas[dato].c08) * this.Mpc.Ponderaciones[7].valor;
				this.Mpc.Empresas[dato].p09 = parseFloat(this.Mpc.Empresas[dato].c09) * this.Mpc.Ponderaciones[8].valor;

				this.Mpc.Empresas[dato].SumaP = (parseFloat(this.Mpc.Empresas[dato].p01) +
												 parseFloat(this.Mpc.Empresas[dato].p02) +
												 parseFloat(this.Mpc.Empresas[dato].p03) +
												 parseFloat(this.Mpc.Empresas[dato].p04) +
												 parseFloat(this.Mpc.Empresas[dato].p05) +
												 parseFloat(this.Mpc.Empresas[dato].p06) +
												 parseFloat(this.Mpc.Empresas[dato].p07) +
												 parseFloat(this.Mpc.Empresas[dato].p08) +
												 parseFloat(this.Mpc.Empresas[dato].p09)).toFixed(2);
			}
			this.CompararEmpresas();
		},
		CompararEmpresas: function(){
			if(this.ponderacionTotal == 1.00){
				var todas = [];
				this.Mpc.Empresas.forEach(element => {
					todas.push({"Nombre": element.Nombre,"Puntaje": parseFloat(element.SumaP)});
				});
				todas.sort(function(a, b){
					return b.Puntaje - a.Puntaje;
				});
				this.Mpc.Merito = [];
				todas.forEach(element => {
					this.Mpc.Merito.push(element);
				});
				this.Mpc.Mensajito = 'Lista de empresas en orden de Puntuación.';
				this.Mpc.ColorValido = '#0008ff';
			}else{
				this.Mpc.Merito = [];
				this.Mpc.Mensajito = "Asegúrese de que la Ponderación Total sea '1'.";
				this.Mpc.ColorValido = 'red';
			}

		},
		// Metodos Amofhit
		CalcularXY: function(){
			var sumaff = 0;
			var sumavc = 0;
			var sumaea = 0;
			var sumafi = 0;

			this.Peyea.Pei.FuFi.forEach(element => {
				sumaff = parseFloat(sumaff) + parseFloat(element.valor);
			});
			this.Peyea.Pei.VeCo.forEach(element => {
				sumavc = parseFloat(sumavc) + parseFloat(element.valor);
			});

			this.Peyea.Pee.EsAm.forEach(element => {
				sumaea = parseFloat(sumaea) + parseFloat(element.valor);
			});

			this.Peyea.Pee.FuIn.forEach(element => {
				sumafi = parseFloat(sumafi) + parseFloat(element.valor);
			});

			this.Peyea.Promedio.FuFi = parseFloat(sumaff/6).toFixed(2);
			this.Peyea.Promedio.VeCo = parseFloat(sumavc/6).toFixed(2);
			this.Peyea.Promedio.EsAm = parseFloat(sumaea/6).toFixed(2);
			this.Peyea.Promedio.FuIn = parseFloat(sumafi/6).toFixed(2);

			this.Peyea.Datos.x = (parseFloat(this.Peyea.Promedio.VeCo) + parseFloat(this.Peyea.Promedio.FuIn)).toFixed(2);
			this.Peyea.Datos.y = (parseFloat(this.Peyea.Promedio.FuFi) + parseFloat(this.Peyea.Promedio.EsAm)).toFixed(2);

			var p = document.getElementById('punto');
			p.style.left = (12 + (this.Peyea.Datos.x * 2) - 0.35) + 'rem';
			p.style.bottom = (12 + (this.Peyea.Datos.y * 2) + 0.4) + 'rem';
		}
	},
	computed:{
		ponderacionTotal: function(){
			let suma = parseFloat(0.00);
			this.Mpc.Ponderaciones.forEach(element => {
				 suma = parseFloat(suma) + parseFloat(element.valor);
			});
			return suma.toFixed(2);
		}
	},
	mounted(){
		this.CalcularXY();
	}
});
